import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

let logs = [];

export default new Vuex.Store({
    state: {
        logged_in: false,
        login_failed_attempts: 0,
        login_log: null,
    },
    getters: {
        logs() {
            return logs;
        }
    },
    mutations: {
        /* sc_state(state, serverState) {
             console.log('mut state', arguments);
             for (let key in serverState) {
                 state[key] = serverState[key];
             }
         },
         sc_authState(state,authState){
             state.authState = authState._;
         },
         sc_item_chat_list(state,data){
             console.log('sc_item_chat_list', data);
             let index = state.chats_list.findIndex(it => it.id === data.id);
             if (index > -1) {
                    state.chats_list[index] = data;
             }else{
                 state.chats_list.push(data);
             }
         },
       /!*  sc_set_counter(state,data){
             state.counter = data;
             console.log('sc_set_counter', state.counter);
         },*!/*/
        sync(state, {field, data}) {
            console.info(`syncing state[${field}] = ${data}`);
            state[field] = data;
            if (field === 'log') {
                logs.push({
                    text: data,
                    date: new Date(),
                });
            }
        }
    },
    actions: {
        sc_hello() {
            console.log('back-end said hello', arguments);
        }
    },
    modules: {}
})
