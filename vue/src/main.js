import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import './plugins/axios'
import './plugins/socket_io'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')


console.log('check', app.$socket);

let onevent = app.$socket.onevent;
app.$socket.onevent = function (packet) {
    let args = packet.data || [];
    onevent.call(this, packet);

    try {
        let [event, ...rest] = args;
        if (event && typeof event === 'string' && event.startsWith('set_')) {
            const field = event.substring('set_'.length, event.length);
            const [data] = rest;
            store.commit('sync', {field, data})
        }
    } catch (e) {
    }
};
