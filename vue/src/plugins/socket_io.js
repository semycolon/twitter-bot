import Vue from 'vue'
import store from '../store'
import VueSocketIO from 'vue-socket.io'
import SocketIOClient from "socket.io-client";

Vue.use(new VueSocketIO({
    debug: true,
    connection: process.env.VUE_APP_BASE_URL,
    vuex: {
        store,
        actionPrefix: 'sc_',
        mutationPrefix: 'sc_'
    },
   // options: { path: "/my-app/" } //Optional options
}))

