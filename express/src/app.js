const express = require('express');
const app = express();

module.exports = app;

module.exports.___init = function () {
  const api = require('./api');
  const cors = require('cors');

  app.use(cors())
  app.use('/api', api);
};