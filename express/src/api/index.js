const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.json('api is working fine!');
})

app.get('/users', async (req, res) => {
  const list = await global?.db?.Users?.find({})?.toArray();
  res.json(list);
})

module.exports = app;
