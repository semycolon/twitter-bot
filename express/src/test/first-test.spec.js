const {closeBrowser} = require("../lib/utils");
const {getBrowser, getNewPage, saveSession, tri} = require("../lib/utils");
const timeout = process.env.SLOWMO ? 30000 : 10000;
const itif = (condition) => condition ? it : it.skip;

async function triWaitFornavigation(page) {
  await tri(async () => await page.waitForNavigation());
}

describe('Test header and title of the page', () => {
  beforeAll(async () => {
    await getBrowser();
  })

  afterAll(async () => {
    //  await closeBrowser();
  })

  let page;
  let loggedIn = false;

  it('should open twitter login page', async function () {
    page = await getNewPage();
    await page.goto('https://twitter.com/login');

    await triWaitFornavigation(page);

    if (page.url() == 'https://twitter.com/home') {
      loggedIn = true;
    } else {
      expect(page.url()).toBe('https://twitter.com/login');
      let title = await page.title();
      console.log('title is', title);
      expect(title).toContain('Twitter');
      expect(title).toContain('Login');
    }
  });

  it('should login', async function () {
    if (loggedIn) return;
    let usernameSelector = 'input[name="session[username_or_email]"]';
    await page.type(usernameSelector, 'semycolon_me', {delay: 10})

    let passwordSelector = 'input[type="password"]';
    await page.type(passwordSelector, 'fucktwitter', {delay: 10})

    const passwordInput = await page.$(passwordSelector);
    await passwordInput.focus();

    await page.keyboard.press('Enter');
    await page.waitForNavigation();

    expect(page.url()).toBe('https://twitter.com/home');
    await saveSession(page)
    await page.close();
    await closeBrowser();
  });

  /*it('should impress user', async function () {
    const user = 'hadi_aliakbar';
    const url = `https://twitter.com/search?q=from%3A%40${user}&src=typed_query&f=live`;
    const page = await getNewPage();
    await page.goto(url);

    expect(page.url()).toBe(url);

   /!* await tri(async () => {
        /!*await page.waitForNavigation({
          waitUntil: 'networkidle0',
        })*!/
        await page.waitForNavigation()
      }
    )*!/

    const likes_count = await page.evaluate(async () => {
      let enough = false;
      let likes_count = 0;
      let last_scroll = 720;
      while(!enough){
        console.log('goint through');
        let likes = document.querySelectorAll('div[data-testid="like"]');
        console.log('likes:', likes);
        for (const key in likes) {
          /!*if (!likes.length){
            enough = true;
            break;
          }*!/
          if (likes_count > 20){
            break;
          }
          try {
            likes[key].click();
            likes_count++;
            console.log('liked',likes_count, likes[key]);
            await new Promise(resolve => setTimeout(resolve, 4000));
          } catch (e) {
            console.error(e);
          }
        }
        last_scroll+=720;
        window.scroll(0, last_scroll);
        console.log('scrolling down');
        await new Promise(resolve => setTimeout(resolve, 10000));
      }
      return Promise.resolve(likes_count);
    });

    expect(likes_count).toBeGreaterThan(0);
  });
  */
/*
  it('should like feed', async function () {
    const url = `https://twitter.com/home`;
    const page = await getNewPage();
    await page.goto(url);

    expect(page.url()).toBe(url);

   /!* await tri(async () => {
        /!*await page.waitForNavigation({
          waitUntil: 'networkidle0',
        })*!/
        await page.waitForNavigation()
      }
    )*!/

    const likes_count = await page.evaluate(async () => {
      let enough = false;
      let likes_count = 0;
      let last_scroll = 720;
      while(!enough){
        console.log('going through');
        let likes = document.querySelectorAll('div[data-testid="like"]');
        let liked = document.querySelectorAll('div[data-testid="unlike"]');
        console.log('liked count', liked.length);
        console.log('likes:', likes);
        for (const key in likes) {
          /!*if (!likes.length){
            enough = true;
            break;
          }*!/
          try {
            likes[key].click();
            likes_count++;
            console.log('liked',likes_count, likes[key]);
            await new Promise(resolve => setTimeout(resolve, 4000));
          } catch (e) {
            console.error(e);
          }
        }
        last_scroll+=720;
        window.scroll(0, last_scroll);
        console.log('scrolling down');
        await new Promise(resolve => setTimeout(resolve, 10000));
      }
      return Promise.resolve(likes_count);
    });

    expect(likes_count).toBeGreaterThan(0);
  });*/

});