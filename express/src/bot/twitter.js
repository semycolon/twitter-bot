import {errors} from "puppeteer";

const Utils = require('../lib/utils');
const SyncedState = require('../lib/SyncedState.js');
const expect = require('expect')
const {tryFail} = require("../lib/utils");
let URL_HOME = 'https://twitter.com/home';
let LOGIN_URL = 'https://twitter.com/login';
const WAIT_UNIT = 5000;
class TwitterBot {
    constructor() {
        this.state = {
            logged_in: Utils.hasCookies(),
            login_failed_attempts: 0,
            login_log: null,
            login_loading: false,
            extractor_active_count: 0,
            impressing_username: [],
            impression_log: null,
            service_impress_running: false,
            log:null,
        };
        this.state = SyncedState(this.state);
        this.pending_impressions = [];
        this.init()
    }

    log(arg) {
        let str = '';
        for (const k in arguments) {
            str += arguments[k] + " -- ";
        }
        this.setState({log: str})
        console.log('LOG:', arg);
    }

    setState(arg) {
        for (const key in arg) {
            this.state[key] = arg[key];
        }
    }

    async init() {
        this.browser = await Utils.getBrowser();
        vue_input.addListener('login', () => this.login(), true);
        vue_input.addListener('logout', () => this.logout(), true);
        vue_input.addListener('start_extract', (arg) => this.extract_followers(arg), true);
        vue_input.addListener('start_impress_service', (arg) => this.startImpressService(), true);
        vue_input.addListener('want_impress', (arg) => this.wantToImpress(arg), true);
        vue_input.addListener('fetch_info', (screen_name) => this.fetchUserFullInfo(screen_name));
        vue_input.addListener('send_dm', (screen_name, messages) => this.sendDM(screen_name, messages));
        vue_input.addListener('dislike', (screenName) => {
            this.dislikeUser(screenName);
        })
        if (!Utils.hasCookies()) {
            await this.login();
        }

        global.responseEmitter.on('body', (body) => {
            this.onResponseBody(body)
        })

        this.log('init complete','second arg','thrid arg')
    }

    async logout() {
        this.log('logging out!');
        await this.browser.close();
        await Utils.deleteCookies();
        this.setState({logged_in: false})
        this.init();
    }

    async login() {
        if (this.state.logged_in) return;
        if (this.state.login_loading) return;
        this.setState({login_loading: true})
        this.log('logging in...');

        await tryFail(async () => {
            const page = await Utils.getNewPage();
            await page.goto(LOGIN_URL);
            if (page.url() !== URL_HOME) {
                expect(page.url()).toBe(LOGIN_URL);
                let title = await page.title();
                expect(title).toContain('Twitter');
                expect(title).toContain('Login');

                let usernameSelector = 'input[name="session[username_or_email]"]';
                await page.type(usernameSelector, 'semycolon_me', {delay: 10})

                let passwordSelector = 'input[type="password"]';
                await page.type(passwordSelector, 'fucktwitter', {delay: 10})

                const passwordInput = await page.$(passwordSelector);
                await passwordInput.focus();

                await page.keyboard.press('Enter');
                await page.waitForNavigation();

                expect(page.url()).toBe(URL_HOME);
            }
            await Utils.saveSession(page)
            await page.close();
        }, {
            name: 'Login',
            onSuccess: () => {
                this.setState({logged_in: true})
                this.log('logged in Successfully.');
            },
            onFailure: () => {
                this.setState({login_failed_attempts: this.state.login_failed_attempts + 1});
                this.log('login failed!')
            },
            onFinal: () => {
                this.setState({login_loading: false})
            },
            printError: true,
        })

    }

    async extract_followers({mode, username, list_name, limit}) {
        const {Users} = global.db;
        this.setState({extractor_active_count: this.state.extractor_active_count + 1})
        try {
            this.log('extract_followers() called', arguments);
            let url = `https://twitter.com/${username}/${mode}`
            const page = await Utils.getNewPage();
            await page.goto(url);

            await page.waitForSelector('main');
            this.log('page is loaded');
            let evalPromise = page.evaluate(function ({limit}) {
                return new Promise(async (resolve, reject) => {
                        try {
                            let links = [];
                            while (true) {
                                console.log('inside loop');
                                let mainElement = document.querySelector('main');
                                let lastHeight = mainElement.clientHeight
                                console.log('going deeper');
                                window.scrollTo(0, lastHeight);

                                let waits = [500, 1600, 2500, 5000, 10000];
                                for (const wait of waits) {

                                    if (mainElement.clientHeight <= lastHeight) {
                                        console.log('waitng for ', wait);
                                        await new Promise(r => setTimeout(r, wait + 5000));
                                    }
                                }
                                console.log('done waiting...');

                                let list = document.querySelectorAll('div[data-testid="UserCell"] a');
                                for (let key in list) {
                                    let it = list[key];
                                    if (it.getAttribute && typeof it.getAttribute === 'function') {
                                        let link = it.getAttribute('href');

                                        if (link.startsWith('http')) {
                                            continue;
                                        }

                                        if (link.indexOf('search?q') >= 0) {
                                            continue
                                        }

                                        link = link.startsWith('/') ? link.substring(1, it.length) : link

                                        links.push({username: link,});
                                    }
                                }

                                console.log('after links for');
                                if (mainElement.clientHeight <= lastHeight || limit && links.length > limit + 200 || !limit && links.length > 200) {
                                    console.log('at the end!');
                                    break;
                                }
                                links = links
                                    .filter((value, index, self) => {
                                        return self.indexOf(value) === index;
                                    })
                            }//end of while
                            console.log('after while');
                            console.log('checkout links', links);
                            links = links
                                .filter((value, index, self) => {
                                    return self.indexOf(value) === index;
                                })
                            resolve(links);
                        } catch (e) {
                            console.error(e);
                        }
                    }
                )
            }, {limit})
            this.log(evalPromise);
            let list = await evalPromise;
            this.log('list :', list);

            let inserted = 0;
            /* for (let user of list) {
                 /!*let it = await Users.findOne({username: user.username})
                 if (!it) {
                   await Users.insertOne(user);
                   inserted++;
                 }*!/
                 await this.fetchUserFullInfo(user.username);
                 await new Promise(r => setTimeout(r, 10000 + Math.random() * 10000));
             }*/
            this.log('db result', inserted);
        } finally {
            this.setState({extractor_active_count: this.state.extractor_active_count - 1})
        }
    }

    async impressUser(screenName) {
        this.log('impressUser called for', screenName);
        if (this.state.impressing_username.length > 0) {
            this.pending_impressions.push(screenName);
            return;
        }
        let page;
        try {

            this.state.impressing_username = [...this.state.impressing_username, screenName];

            const User = global.getMongooseModel('User');
            const user = await User.findOne({screenName})

            if (!user) {
                this.log('no such user in database', screenName);
                return;
            }

            if (user && user.impressed) {
                this.log('already impressed ' + screenName.bgRed.white);
                // return;
            }

            let url = `https://twitter.com/${screenName}`;
            this.log('Impressing user', screenName);
            page = await Utils.getNewPage();
            await page.goto(url);

            await page.waitForSelector('div[data-testid="placementTracking"]', {timeout: 10000});

            //follow user
            if (!user.following) {
                let following;
                following = await this.followUser(page);
                user.following = following;
            }

            this.log(user.following ? 'is' : 'not', ' following ', screenName);
            let likes = await page.evaluate(function () {
                return new Promise(async (resolve, reject) => {
                    let likes = 0;
                    while (true) {
                        console.log('likes inside loop, going deeper');
                        let mainElement = document.querySelector('main');
                        let lastHeight = mainElement.clientHeight
                        window.scrollTo(0, lastHeight);

                        //scroll down
                        let waits = [500, 1600, 2500, 5000, 10000];
                        for (const wait of waits) {
                            if (mainElement.clientHeight <= lastHeight) {
                                await new Promise(r => setTimeout(r, wait * 2));
                            }
                        }

                        let tweets = document.querySelectorAll('div[data-testid="tweet"]');
                        for (let key in tweets) {
                            let it = tweets[key];
                            if (it.querySelector && typeof it.querySelector === 'function') {
                                let likeBtn = it.querySelector('div[data-testid="like"]');
                                if (likeBtn) {
                                    likeBtn.click();
                                    likes++;
                                    if (likes > 13) {
                                        break;
                                    }
                                    console.log('Liked a tweet', likes);
                                    let timeout1 = 10000 + Math.random() * 10000;
                                    console.log('waiting for', timeout1);
                                    await new Promise(r => setTimeout(r, timeout1));
                                }
                            }
                        }

                        if (mainElement.clientHeight <= lastHeight || likes > 6) {
                            console.log('like at the end of while!');
                            break;
                        }
                    }//end of while
                    resolve(likes);
                })
            });

            this.log('liked ', likes, ' for ', screenName);

            await User.updateOne({screenName}, {
                $set: {
                    likes: user.likes ? user.likes + likes : likes,
                    impressed: true,
                    impressed_at: new Date(),
                    pending_impress: false,
                }
            })
            let timeout = WAIT_UNIT + Math.random() * 60000 * 2.5;
            this.log('waiting after impression', screenName,timeout);
            await new Promise(re => setTimeout(re, timeout))
        } catch (e) {
            console.log('Error(impress user):', e.message);
            console.error(e);
        } finally {
            try {
                this.state.impressing_username = this.state.impressing_username.filter(it => it !== screenName);
                await page?.close?.();

                if (this.pending_impressions.length > 0) {
                    let newTarget = this.pending_impressions[0];
                    this.pending_impressions = this.pending_impressions.filter(it => newTarget !== it);
                    await this.impressUser(newTarget);
                }
            } catch (e) {
            }
        }
    }

    async followUser(page) {
        return (await page.evaluate(function () {
            return new Promise(async (resolve, reject) => {
                try {
                    let followBtn = document.querySelector('div[data-testid="placementTracking"]').querySelector('div[role="button"]');
                    if (!followBtn) {
                        console.error('follow ridi');
                        resolve(false)
                    }
                    let text = followBtn.innerText;
                    if (text === 'Follow') {
                        console.log('wait before follow');
                        await new Promise(r => setTimeout(r, 5000 + Math.random() * 10000));
                        followBtn.click();
                        console.log('wait after follow');
                        await new Promise(r => setTimeout(r, 2000));
                        resolve(followBtn.innerText === 'Following')
                    } else {
                        resolve(true);
                    }
                } catch (e) {
                    console.error('ridi catch follow btn')
                    console.error(e);
                }
            })
        }));
    }

    async sendDM(username, messages) {
        const page = await Utils.getNewPage();
        const url = `https://twitter.com/${username}`
        await page.goto(url);
        await page.waitForSelector('main');

        const canSendDM = await page.evaluate(function () {
            return new Promise((resolve, reject) => {
                let dmBtn = document.querySelector('div[data-testid="sendDMFromProfile"]');
                if (dmBtn && dmBtn.click) {
                    console.log('clicked on dm button');
                    dmBtn.click();
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
        });
        this.log('Can Send DM?', "Can Send DM?", canSendDM);

        if (canSendDM) {
            // this.log('waiting for navigation');
            //await page.waitForNavigation();
            this.log('waiting for selector');
            await page.waitForSelector('div[data-testid="dmComposerTextInput"]')
            await page.evaluate(function (list) {
                return new Promise(resolve => {
                    let parentInput = document.querySelector('div[data-testid="dmComposerTextInput"] span').parentElement;
                    /*     parentInput.innerHTML = "";
                         list.forEach(msg =>{
                           let span = document.createElement('span');
                           span.setAttribute('data-text', 'true');
                           span.innerHTML = msg;
                           parentInput.appendChild(span);
                           parentInput.appendChild(document.createElement('br'));
                         });*/
                    parentInput.click();
                    resolve();
                    /*const sendBtn = document.querySelector('div[data-testid="dmComposerSendButton"]')
                    sendBtn.removeAttribute('disabled');
                    sendBtn.removeAttribute('aria-disabled');
                    sendBtn.className = "";
                    sendBtn.click();*/

                })
            }, messages);
            for (let message of messages) {
                await page.keyboard.type(message, {delay: 2000})
                await page.keyboard.press('Enter');
            }
        }

    }

    async fetchUserFullInfo(username) {
        this.log('fetchUserFullInfo', username);
        const url = `https://twitter.com/${username}`;
        const page = await Utils.getNewPage();
        await page.goto(url);
        try {
            await page.waitForSelector('div[data-testid="tweet"]', {timeout: 10000});
        } catch (e) {
            console.error('ridi ', 'fetchUserFullInfo', 'waitForSelector(\'div[data-testid="tweet"]\',');
            console.error(e);
        }
        this.log('closing page');
        try {
            await page.close();
        } catch (e) {
        }
    }

    async onResponseBody(body, request) {
        const utils = require("../lib/utils");
        const {insertUserToDB} = utils;
        try {
            if (body?.users?.length > 0 && Array.isArray(body?.users)) {
                for (const user of body.users) {
                    await insertUserToDB(user);
                }
            }

            if (body?.inbox_initial_state?.users && Array.isArray(body.inbox_initial_state.users)) {
                for (const user of body.inbox_initial_state.users) {
                    await insertUserToDB(user);
                }
            }
            if (body?.globalObjects?.users && Array.isArray(body.globalObjects.users)) {
                for (const user of body.globalObjects.users) {
                    await insertUserToDB(user);
                }
            }

            if (body?.data?.user) {
                await insertUserToDB(body.data.user);
            }

            let instructions = body?.data?.user?.following_timeline?.timeline?.instructions;
            if (!instructions) instructions = body?.data?.user?.followers_timeline?.timeline.instructions;
            if (instructions) {
                for (const instructionsKey in instructions) {
                    let instruction = instructions[instructionsKey];
                    if (instruction.type === 'TimelineAddEntries') {
                        for (let key in instructions[instructionsKey].entries) {
                            let it = instructions?.[instructionsKey]?.entries?.[key];
                            let userrr = it?.content?.itemContent?.user;
                            if (userrr) {
                                await insertUserToDB(userrr);
                            }
                        }
                    }
                }
            }

            if (Array.isArray(body) && body.length > 0 && body[0].user_id) {
                for (let item of body) {
                    if (item.user) {
                        await insertUserToDB(item.user);
                    }
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    async startImpressService() {
        this.log('starting impress service');
        this.state.service_impress_running = true;
        const User = getMongooseModel('User');

        try {
            while (this.state.service_impress_running) {
                let pendingUsersList = await User.find({pending_impress: true});
                if (pendingUsersList.length > 0) {
                    for (const user of pendingUsersList) {
                        if (user.screenName) {
                            await this.impressUser(user.screenName);
                        } else {
                            console.error('user.screenName wass null');
                        }
                    }
                } else {
                    this.log('No Pending user... stopping service');
                    this.state.service_impress_running = false;
                }
            }
        } finally {
            this.state.service_impress_running = false;
        }
    }

    async wantToImpress(screenName) {
        this.log('want to impress', screenName);
        const User = getMongooseModel('User');
        const user = await User.findOne({screenName})
        if (user) {
            await User.updateOne({screenName}, {$set: {pending_impress: true}})
            if (!this.state.service_impress_running) {
                await this.startImpressService();
            } else {
                this.log('service is already running...')
            }
        } else {
            this.log('user not found with screenName:', screenName);
        }
    }

    async dislikeUser(screenName) {
        const User = getMongooseModel('User');
        await User.updateOne({screenName}, {$set: {dislike: true,}})
    }
}

module.exports = TwitterBot;