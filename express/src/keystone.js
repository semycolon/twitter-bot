import {Checkbox, Integer, DateTime, Text,} from '@keystonejs/fields';
import {isString, isNumber, isNumeric, isBool,} from 'typeof-utility';

const {MongooseAdapter: Adapter} = require('@keystonejs/adapter-mongoose');
const PROJECT_NAME = 'Twitter Bot';
const expressSession = require('express-session');
const MongoStore = require('connect-mongo')(expressSession);

const onConnect = (keystone) => {

};

const {Keystone} = require('@keystonejs/keystone');
global.getMongooseModel = function (listName) {
    return global?.keystone?.adapters?.MongooseAdapter?.mongoose?.models[listName]
}

export function CreateKeystone() {
    let keystone = new Keystone({
            adapter: new Adapter({mongoUri: process.env.MONGO_URI}),
            cookieSecret: process.env.COOKIE_SECRET || 'SET_COOKIE_SECRET',
            sessionStore: new MongoStore({url: process.env.MONGO_URI}),
            onConnect,
            cookie: {
                secure: false, // Default to true in production
                maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
                sameSite: false,
            },
        }
    );
    global.keystone = keystone;
    return keystone
}

export async function initModels(keystone) {
    const user = await global.db.Users.findOne({});
    if (!user) {
        console.error('init models rid, no user in the database');
        keystone.createList('User', {
            fields: {
                name: {
                    type: Text,
                }
            }
        })

    }else{
        let fields = {};
        for (let userKey in user) {
            if (userKey.startsWith('_')) {
                userKey = 'a' + userKey
            }

            const value = user[userKey];

            switch (true) {
                case isBool(value): {
                    fields[userKey] = {
                        type: Checkbox,
                    }
                    break;
                }
                case isString(value) : {
                    fields[userKey] = {
                        type: Text,
                    }
                    break;
                }
                case isNumeric(value) : {
                    fields[userKey] = {
                        type: Integer,
                    }
                    break;
                }
            }
        }

        keystone.createList('User', {
            fields, access: () => {
                console.log('haha');
                return true;
            },
        })
    }

    keystone.adapters.MongooseAdapter.listAdapters.User.schema.options.validateBeforeSave
    keystone.adapters.MongooseAdapter.listAdapters.User.schema.options.strict = false
}