const debug = require('debug')('SyncedState')
module.exports = function (initialState) {

  global.io.on('connection', (socket) => {
    for (const key in initialState) {
      socket.emit('set_' + key, initialState[key]);
      debug(`socket connected, syncing ${key}  ${initialState[key]}`);
      socket.on('set_' + key, (val) => {
        console.log('setting value from client side', key, val);
        initialState[key] = val;
        socket.emit('set_' + key, val);
      })
    }
  })


  return new Proxy(initialState, {
    set(currentState, prop, value) {
      try {
        currentState[prop] = value;
        global.io.emit('set_' + prop, value);
        debug('state changed, syncing data:', prop, initialState[prop]);
        return true;
      } catch (e) {
        console.error(e);
      }
    },
  });
}