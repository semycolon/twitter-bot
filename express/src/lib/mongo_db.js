const { MongoClient } = require("mongodb");
// Replace the uri string with your MongoDB deployment's connection string.
const uri = process.env.MONGO_URI;
console.log('uri is:', uri);
const client = new MongoClient(uri);
async function init_db() {
    try {
        await client.connect();
        const database = client.db('Twitter_Bot');
        const Users = database.collection('users');

        return {Users};
    } finally {
        // Ensures that the client will close when you finish/error
        //await client.close();
    }
}

module.exports = {init_db}
