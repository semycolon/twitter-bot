const debug = require('debug')('vue-input');

const EventEmitter = require('eventemitter2');
const emitter = new EventEmitter({wildcard: true});

global.io.on('connection', socket => {
    debug('got new socket');
    let onevent = socket.onevent;
    socket.onevent = function (packet) {
        let args = packet.data || [];
        const [event, data, ...rest] = args;
        onevent.call(this, packet);
        debug('got event:', event, ' data:', data);
        emitter.emit(event, data);
    };
})

emitter.on('*', function (value1) {
    debug('VueInput got event:', this.event, value1);
});

global.vue_input = {
    waitForVueInput(key) {
        debug('waiting for ', key);
        return emitter.waitFor(key)
            .then(res => {
                debug('done with waiting', key);
                const [data] = res;
                return Promise.resolve(data);
            })
    },
    addListener(key,handler,removeExistingListener = false){
        debug('added listener',key);
        let mHandler = function (value) {
            handler(value);
        };
        if(removeExistingListener) emitter.removeListener(key, mHandler);
        emitter.on(key, mHandler);
    }
}
