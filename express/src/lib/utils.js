const colors = require('colors');
const fs = require('fs');
const SyncedState = require('../lib/SyncedState.js');
let cookiesFilePath = require('path').join(__dirname, '../cookies.json');
let localStoragePath = require('path').join(__dirname, '../localStorage.json');
const puppeteer = require('puppeteer');
let browser = null;
let flag429 = false;

let state = {
  browser_open:false,
  last_429:null,
  headless: Boolean(process.env.HEADLESS),
}

state = SyncedState(state);
const EventEmitter = require('eventemitter2');
global.responseEmitter = new EventEmitter({wildcard: true});

async function getBrowser() {
  if (!browser) {
    browser = await puppeteer.launch({headless: state.headless, defaultViewport: {width: 1280, height: 720,}});

    browser.on('disconnected', () => {
      // console.log("event: 'disconnected'")
      browser = null;
      state.browser_open = false;
    })    // Emitted when Puppeteer gets disconnected from the Chromium instance.
    browser.on('targetchanged', () => {
      // console.log("event: 'targetchanged'")
    })   // Emitted when the url of a target changes.
    browser.on('targetcreated', () => {
      // console.log("event: 'targetcreated'")
    })   // Emitted when a target is created, for example when a new page is opened by window.open or browser.newPage.
    browser.on('targetdestroyed', () => {
      // console.log("event: 'targetdestroyed'")
    }) // Emitted when a target is destroyed, for example when a page is closed.

    state.browser_open = true;
  }
  return browser;
}

async function saveSession(page) {
// Save Session Cookies
  try {
    const cookiesObject = await page.cookies()
// Write cookies to temp file to be used in other profile pages
    fs.writeFile(cookiesFilePath, JSON.stringify(cookiesObject, null, '\t'),
      function (err) {
        if (err) {
          console.log('The file could not be written.', err)
        }
        console.log('Session has been successfully saved')
      })
  } catch (e) {
    console.error(e)
  }

  try {
    const localStorage = await page.evaluate(function () {
      return new Promise((resolve, reject) => {
        try {
          console.log('localstorage is', localStorage, Object.keys(localStorage));
          const keys = Object.keys(localStorage);
          let obj = {};
          console.log('keys are', keys, typeof keys);
          for (let key in localStorage) {
            console.log('found local Storage', key);
            if (localStorage[key] && typeof localStorage[key] === 'function') {

            } else {
              obj[key] = localStorage[key];
              console.log('yeah', key, localStorage[key]);
            }
          }
          resolve(obj);
        } catch (e) {
          console.error('ridi localStorage')
          console.error(e);
          resolve({});
        }
      })
    });
    fs.writeFileSync(localStoragePath, JSON.stringify(localStorage, null, '\t'), {encoding: 'utf-8',})
  } catch (e) {
    console.error(e)
  }
}

async function deleteCookies() {
  try {
    if (fs.existsSync(cookiesFilePath)) {
      fs.unlinkSync(cookiesFilePath);
    }
  } catch (e) {
    console.error(e);
  }
}

async function loadSession(page) {
  const previousSession = fs.existsSync(cookiesFilePath)
  if (previousSession) {
    // If file exist load the cookies
    const cookiesString = fs.readFileSync(cookiesFilePath, {encoding: 'utf-8'});
    const parsedCookies = JSON.parse(cookiesString);
    if (parsedCookies.length !== 0) {
      for (let cookie of parsedCookies) {
        await page.setCookie(cookie)
      }
      console.log('Session has been loaded in the browser')
    }
  }
}

async function loadLocalStorage() {
  const hasLocalStorage = fs.existsSync(localStoragePath);
  if (hasLocalStorage) {
    const lsString = fs.readFileSync(localStoragePath, {encoding: 'utf-8'})
    const ls = JSON.parse(lsString);
    if (ls) {
      await page.evaluate((obj) => {
        console.log('loading locaclStorage');
        for (let key in obj) {
          localStorage.setItem(key, obj[key]);
          console.log('set localStorage', key, obj[key])
        }
      }, ls)
    }
  }
}

const insertUserToDB = async function (user) {
  const {id, rest_id, legacy, affiliates_highlighted_label, legacy_extended_profile} = user;
  if (!id || !rest_id) {
    return;
  }
  let User = getMongooseModel('User');

  let existing = await User.findOne({twitterId:id});
  let payload = {
    twitter_id:id,
    rest_id:rest_id,
    ...legacy,
    ...affiliates_highlighted_label,
    ...legacy_extended_profile
  };
  for (const key in payload) {
    if (key.includes('_')) {
      let newFieldName = String(key);
      while (newFieldName.indexOf('_') >= 0) {
        let index = newFieldName.indexOf('_');
        let before = newFieldName.substring(0,index)
        let letter = newFieldName.substring(index+1,index+2);
        let rest = newFieldName.substring(index + 2);
        newFieldName = before+letter.toUpperCase()+rest;
       // console.log('newFieldName', newFieldName);
      }
      payload[newFieldName] = payload[key];
      delete payload[key];
    }
  }
  if (existing) {
    console.log('Updating user info'.bgGreen.blue);
    for (const key in payload) {
      existing[key] = payload[key];
    }
    await existing.save();
  } else {
    console.log('Inserting new user'.bgGreen.blue);
    await new User(payload).save();
  }
}

async function getNewPage() {
  let b = await getBrowser();
  let page = await b.newPage();

  page.on('request', () => {
    // console.log('page-event:' + 'request')
  })          // Emitted when a page issues a request.
  page.on('requestfailed', () => {
    // console.log('page-event:' + 'requestfailed')
  })    // Emitted when a request fails, for example by timing out.
  page.on('requestfinished', () => {
    // console.log('page-event:' + 'requestfinished')
  })  // Emitted when a request finishes successfully.
  page.on('response', async (response) => {
    // console.log('page-event:' + 'response');

    try {
      if (response.status() == 429) {
        browser.close();
        flag429 = true;
        state.browser_open = false;
        state.last_429 = new Date();
      }
    } catch (e) {
      console.error(e);
    }

    try {
      let body = null;
      try {
        body = await response.json();
      } catch (e) {

      }
      if (body) {
        responseEmitter.emit('body',body,response);
      }
    } catch (e) {
      console.error(e);
    }
  })         // Emitted when a response is received.
  page.on('workercreated', () => {
    // console.log('page-event:' + 'workercreated')
  })

  page.on('console', message => {
    console.log('  CONSOLE  '.red.bgYellow + `  ${message.type().substr(0, 3).toUpperCase()}  `.bgGreen.red + ` ${message.text()}`)
  })

  await loadSession(page);
  return page;
}

async function closeBrowser() {
  await (await getBrowser()).close()
}

async function tri(arg) {
  try {
    if (arg && typeof arg === 'function') {
      arg();
    }
  } catch (e) {

  }
}

function hasCookies() {
  return fs.existsSync(cookiesFilePath);
}

const Helpers = {
  getBrowser, getNewPage, closeBrowser, saveSession, tri, deleteCookies, loadLocalStorage, hasCookies,insertUserToDB
}
module.exports = Helpers;

module.exports.tryFail = async function (block, {name, onSuccess, onFailure, timeout, onFinal, printError} = {name: ''}) {
  try {
    let timeout_id;
    if (timeout) {
      timeout_id = setTimeout(() => {
        throw new Error(name + ' Timed Out...');
      });
    }
    const result = await block();
    if (onSuccess) {
      onSuccess(result);
    }
    if (timeout_id) {
      clearTimeout(timeout_id)
    }
  } catch (e) {
    if (printError) {
      console.error(e);
    }
    if (onFailure) {
      onFailure(e);
    }
  } finally {
    if (onFinal) {
      onFinal();
    }
  }
};


global.vue_input.addListener('open_browser',getBrowser);
global.vue_input.addListener('close_browser',()=>{
  if (browser) {
    browser.close();
  }
});


