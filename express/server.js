const path = require('path');
require('dotenv').config({path: path.join(__dirname, '.env')})


import {initModels, CreateKeystone} from "./src/keystone";

const {AdminUIApp} = require('@keystonejs/app-admin-ui');


const {GraphQLApp} = require('@keystonejs/app-graphql');
const http = require('http');
const app = require('./src/app.js')
const server = http.createServer(app);
const {PORT} = process.env;

if (!PORT) throw new Error("PORT is not available in the env");
const {version} = process;
if (!version.startsWith('v10')) {
    throw new Error('you must user node.js version 10');
}


const io = getIo();
global.io = io;
require('./src/lib/VueInput.js');


(async () => {

    const keystone = CreateKeystone();
    const {init_db} = require('./src/lib/mongo_db');
    const {Users} = await init_db()
    global.db = {Users}
    await initModels(keystone)

    keystone
        .prepare({
            apps: [new GraphQLApp({
                apiPath: "/gql",
                graphiqlPath: "/gql/playground"
            }),
                new AdminUIApp({
                    name: process.env.PROJECT_NAME || 'keystone',
                    enableDefaultRoute: true,
                    apiPath: '/gql/api'
                    // authStrategy,
                    //  hooks: require.resolve('./src/admin/hooks.js'),
                }),
            ],
            dev: true,
            pinoOptions: {
                prettyPrint: {
                    levelFirst: true
                }
            }
        })
        .then(async ({middlewares}) => {
            global.keystone = keystone;

            await keystone.connect();
            app.use(middlewares);

            server.listen(PORT, async () => {
                app.___init();
                console.log('server is listening on http://localhost:' + PORT);
                const TwitterBot = require('./src/bot/twitter.js');
                const bot = new TwitterBot()
            });
        });
})()


function getIo() {
    return require('socket.io')(server, {
        handlePreflightRequest: function (req, res) {
            var headers = {
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                'Access-Control-Allow-Origin': 'http://localhost:8081',
                'Access-Control-Allow-Credentials': true
            };
            res.writeHead(200, headers);
            res.end();
        },
        cors: {
            origin: "http://localhost:8081",
            methods: ["GET", "POST"],
            credentials: true,
        }
    });
}